//
//  ClickableView.swift
//  Virtual Affairs Assessment - Views
//
//  Created by Terrick Mansur on 12/4/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ClickableView: UIView {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        //self.backgroundColor=UIColor.lightGray//Color when UIView is clicked.

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        //self.backgroundColor=UIColor.lightGray
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        //self.backgroundColor=UIColor.white
    }
}
