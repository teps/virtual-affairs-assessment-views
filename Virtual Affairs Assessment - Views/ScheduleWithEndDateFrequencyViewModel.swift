//
//  ScheduleWithEndDateFrequencyViewModel.swift
//  Virtual Affairs Assessment - Views
//
//  Created by Terrick Mansur on 12/2/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleWithEndDateFrequencyViewModel: ScheduleViewModel {

    /**
     * The enum represents all the posible Frequency the ViewModel accpets
     */
    public enum Frequency : String{
        case Once = "Once"
        case Daily = "Daily"
        case Weekly = "Weekly"
        case Monthly = "Monthly"
    }

    //These are al the options rar values in an array
    let OPTIONS = [Frequency.Once.rawValue, Frequency.Daily.rawValue, Frequency.Weekly.rawValue, Frequency.Monthly.rawValue]
    
    //The values for each case
    let FREQUENCY_VALUES = [Frequency.Once:0, Frequency.Daily:1, Frequency.Weekly:2,Frequency.Monthly:3]

    //The string representations of out values
    public private(set) var frequencyStringValue: String = "Once"

    //The string representations of out values
    public private(set) var beginDatePlusFrequencyString : String = ""

    //The string representations of out values
    public private(set) var selectedEndString : String = ""

    //The default value for the frequency is once
    var frequency = Frequency.Once

    //This is the date that is the begin date plus the frequency, it is null because if the frequency if Once, it should be null
    public private(set) var beginDatePlusFrequency : Date?

    //This is the date that is the begin date plus the frequency, it is null because if the frequency if Once, it should be null
    public private(set) var endDate : Date?
    
    //Below are a set of bools that tells our viewcontroller if the given value is editing or not
    public private(set) var editingBeginDate: Bool = false
    public private(set) var editingFrequencyDate: Bool = false
    public private(set) var editingEndDate: Bool = false

    /**
     * This function attempts to set the frequency of the schedule
     *
     * @param: The frequency the user would like to set
     */
    func setFrequency(frequency: Frequency){
        editingFrequencyDate=false
        //Set the frequency
        self.frequency = frequency;
        //Lets recalculate the beginDateplusFrequency
        self.beginDatePlusFrequency=calculateBeginDatePlusFrequency(date: beginDate, frequency: frequency)
        //If the frequency is nill, set the end date to nil
        if frequency == Frequency.Once {
            self.endDate=nil
        }
        else if endDate == nil{
            setEndDate(endDate: beginDatePlusFrequency!)
        }
        else{
            //Check if the end date is still a valid value
            checkEndDateWithBeginDatePlusFreq();
        }
        //Alert the delegate that the frequency has changed
        updateFrequencyStringEndDateString();
    }
    
    /**
     * This function attempts to set the frequency with an Int value
     *
     * @param: The frequency the user would like to set
     */
    func setFrequency(frequencyInt: Int){
        if frequencyInt == 0{
            self.setFrequency(frequency: Frequency.Once)
        }
        else if frequencyInt == 1{
            self.setFrequency(frequency: Frequency.Daily)
        }
        else if frequencyInt == 2{
            self.setFrequency(frequency: Frequency.Weekly)
        }
        else if frequencyInt == 3{
            self.setFrequency(frequency: Frequency.Monthly)
        }
    }
    
    /**
     * We need some extra functionalities in the set begin date
     *
     * @param: The date we woud like to set
     */
    override func attemptSetBeginDate(setDate: Date) {
        super.attemptSetBeginDate(setDate: setDate)
        //Lets recalculate the beginDateplusFrequency
        self.beginDatePlusFrequency=calculateBeginDatePlusFrequency(date: beginDate, frequency: frequency)
        //Check if the end date is still a valid value
        checkEndDateWithBeginDatePlusFreq();
        //We have set out begin date, stop editing
        self.editingBeginDate=false
        //Alert the delegate that values have changed
        alertDelegate()
    }
    
    /**
     * This function will attempt to set the end date of the viewmodel. The end date cannot be set to a date befor begindate plus frequency. This check is the main purpose of this function.
     *
     * @param: The end date that you would like to set to this modelview
     */
    func setEndDate(endDate: Date){
        //Check is the end date if before the beginDatePlusFrequency
        if let beginDatePlusFrequency = beginDatePlusFrequency{
            if beginDatePlusFrequency.compare(endDate) == ComparisonResult.orderedAscending{
                self.endDate = endDate
            }
            else{
                self.endDate=beginDatePlusFrequency
            }
            //Update the string values
            updateFrequencyStringEndDateString()
        }
        //We have set out end date, stop editing
        editingEndDate=false
        //
        alertDelegate()
    }
    
    /**
     * This function will clear all the values and set them to the default values
     */
    func clear(){
        //Set the frequency to once
        setFrequency(frequency: Frequency.Once)
        //Set the begin date to today
        attemptSetBeginDate(setDate: Date())
        //Update the strings
        updateFrequencyStringEndDateString()
    }
    
    /**
     * The index position of the frequency we are currently on
     *
     * @return an int representing the index our frequency is on
     */
    func frequencyIndexPosition() -> Int{
        return FREQUENCY_VALUES[self.frequency]!
    }
    
    /**
     * Call this function when you want to start editing the frequency
     */
    func openEditFrequency(){
        //Set the editing frequency to true and all else to false
        self.editingFrequencyDate = true
        self.editingBeginDate = false
        self.editingEndDate = false
        //Alert the delegate that we have new values
        alertDelegate();
    }
    
    /**
     * Tell the view model to permit us to edit the begin date
     */
    func openEditBeginDate(){
        //Set the editing begin date to true and all else to false
        self.editingFrequencyDate = false
        self.editingBeginDate = true
        self.editingEndDate = false
        //Alert the delegate that we have new values
        alertDelegate();
    }

    /**
     * Tell the view model to permit us to edit the begin date
     */
    func openEditEndDate(){
        //Set the editing begin date to true and all else to false
        self.editingFrequencyDate = false
        self.editingBeginDate = false
        self.editingEndDate = true
        //Alert the delegate that we have new values
        alertDelegate();
    }
    
    /**
     * This function will reset the end date to the begin date plus freq if its before it
     */
    private func checkEndDateWithBeginDatePlusFreq(){
        //Lets update the end date. If its befire the now beginDatePlusFreq, we need to update it
        if endDate?.compare(beginDatePlusFrequency!) == ComparisonResult.orderedAscending{
            setEndDate(endDate: beginDatePlusFrequency!)
        }
    }
    
    /**
     * This function updates the string representations os the frequency and the EndDate
     */
    private func updateFrequencyStringEndDateString(){
        //Update the frequency string
        frequencyStringValue=frequency.rawValue
        //Also update the string for the begindate plus frequency
        if let beginDatePlusFrequency = beginDatePlusFrequency{
            beginDatePlusFrequencyString = DateUtil.convertDateToString(date: beginDatePlusFrequency, format: DATE_FORMAT)
        }
        if let endDate = self.endDate{
            selectedEndString = DateUtil.convertDateToString(date: endDate, format: DATE_FORMAT)
        }
        //We updates some valeus that our viewcontroller uses, notify it
        alertDelegate();
    }
    
    /**
     * This function will calculate the given date plus the frequency passed in by it and return the calculation
     *
     * @param: The frequency 
     * @param: The date you would like to add the frequency to
     */
    private func calculateBeginDatePlusFrequency(date: Date, frequency: Frequency) -> Date?{
        switch frequency {
        case Frequency.Once://If its only once, set it to nil
            return nil
        case Frequency.Daily:
            return DateUtil.addDaysToDate(days: 1, date: beginDate)
        case Frequency.Weekly:
            return DateUtil.addWeekdToDate(weeks: 1, date: beginDate)
        case Frequency.Monthly:
            return DateUtil.addMonthsToDate(months: 1, date: beginDate)
        }
    }
    
}
