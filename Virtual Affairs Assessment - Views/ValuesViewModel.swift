//
//  ValueTableViewCellViewModel.swift
//  Virtual Affairs Assessment - Views
//
//  Created by Terrick Mansur on 12/2/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ValuesViewModel: ViewModel {
    
    //The string to the left
    var leftString = ""
    //The string to tht right
    var rightString = ""
    
    /**
     * Init the object with the given values
     *
     * @param: The lest string value
     * @param: The right string value
     */
    init(leftString: String, rightString: String){
        //Set the values
        self.leftString=leftString
        self.rightString=rightString
    }
    
    /**
     * This function sets the values and informs the delegate about the changes
     *
     * @param: The lest string value
     * @param: The right string value
     */
    func setValues(leftString: String, rightString: String){
        //Set the values
        self.leftString=leftString
        self.rightString=rightString
        //Alert the delegate
        alertDelegate()
    }
}
