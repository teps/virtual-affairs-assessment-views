//
//  ViewModel.swift
//  Virtual Affairs Assesment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

/**
 * The delegate class that defines the protocol for the observers to observe the ViewModel class
 */
protocol ViewModelDelegate: class{
    
    /**
     * Implementing this function will notify that the beginDateString values or/and the endDateStringValue has changed
     */
    func valuesDidChange();
}

class ViewModel: NSObject {

    //The delegate class that The view Controllers will need to assign
    internal weak var delegate : ViewModelDelegate?
    
    /**
     * This function will check is a delegate is set, if one is we will alert it that values have changed.
     */
    internal func alertDelegate(){
        //Now that we updated the values, we need to tell our delegate about this, if we have one
        if let delegate = self.delegate{
            delegate.valuesDidChange()
        }
    }
}
