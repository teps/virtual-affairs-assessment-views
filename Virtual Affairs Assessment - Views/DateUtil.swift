//
//  Util.swift
//  Virtual Affairs Assesment
//
//  Created by Terrick Mansur on 11/30/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class DateUtil: NSObject {

    /**
     * This function convert the given date to a string with the given format
     *
     * @param The date we want a string representation of
     * @param The format we want this representation in
     */
    static func convertDateToString(date: Date, format: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //Return the string representation of the passed in date
        return dateFormatter.string(from: date)
    }

    /**
     * This function take in an amount of days to add to a date and a Date to add these days to. It return a new Date object that is the set days ahead of the one passed in.
     *
     * @param The days you would like to add the the passed in date
     * @param The Date that you would like to add the days to
     * @return The new Date with the added days
     */
    static func addDaysToDate(days :Int, date :Date) -> Date?{
        //Create the date companents object
        var dateComponents = DateComponents()
        //Set the amount of days that we will be adding
        dateComponents.day = days
        //Return the new date with the added days
        return Calendar.current.date(byAdding: dateComponents, to: date)
    }
    
    /**
     * This function will take in an amount of weeks and add the given amount of months to it
     *
     * @param The weeks you would like to add the the passed in date
     * @param The Date that you would like to add the weeks to
     * @return The new Date with the added weeks
     */
    static func addWeekdToDate(weeks: Int, date: Date) -> Date?{
        //Create the date companents object
        var dateComponents = DateComponents()
        //Set the amount of days that we will be adding
        dateComponents.weekOfMonth = weeks
        //Return the new date with the added days
        return Calendar.current.date(byAdding: dateComponents, to: date)
    }
    
    /**
     * This function will take in an amount of months and add the given amount of months to it
     *
     * @param The months you would like to add the the passed in date
     * @param The Date that you would like to add the months to
     * @return The new Date with the added months
     */
    static func addMonthsToDate(months: Int, date: Date) -> Date?{
        //Create the date companents object
        var dateComponents = DateComponents()
        //Set the amount of days that we will be adding
        dateComponents.month = months
        //Return the new date with the added days
        return Calendar.current.date(byAdding: dateComponents, to: date)
    }
}
