//
//  ScheduleViewModel.swift
//  Virtual Affairs Assesment
//
//  Created by Terrick Mansur on 11/28/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleViewModel: ViewModel {

    //The date format that we will be using to display the dates
    let DATE_FORMAT="dd MMMM yyyy"
    
    //The properties/values that the ScheduleViewController will be displaying to the user
    public private(set) var beginDateStringValue : String = "Not set";
    public private(set) var endDateStringValue : String = "Not set";
    
    //The values that we will need for the business logic of the ViewModel
    //The begin date ta display
    public private(set) var beginDate = Date()
    
    //The earliest the viewcontroller may select
    public private(set) var earliestDate = Date()
    
    //The amount of days between the begin date and end date
    private let durationInDays = 7
    
    //The time zone we are currently in we will set it is we can
    private var timeZone : TimeZone?

    /**
     * The default init function should set the beginDateString to the string representation of todays date. The earliestDate and begin date are all set the the current time by the default constructor of the Date object. The day gap is already set to 7 as default. The init also sets the current time zone the device is in.
     */
    override init(){
        //Call the super init
        super.init()
        
        //Set the time zone that we are currently in if we can
        if let abbreviation = NSTimeZone.local.abbreviation(){
            setTimeZone(abbreviation: abbreviation)
        }
        
        //Update the values for the BeginDateString and the EndDate String
        updateBeginDateEndDateStringValues()
    }
    
    /**
     * The set delegate class allows anyone who implements the ScheduleViewModelDelegate protocol to set its reference to our delegate
     *
     * @param Any class who implements ScheduleViewModelDelegate protocol
     */
    public func setDelegate(toSetDelegate: ViewModelDelegate){
        self.delegate = toSetDelegate;
    }
    
    /**
     * This function will attempt to set the date passed in. It will check is the date is in the passed or not (compare it with earliest date). After the dates are set, this function will update the date string values.
     */
    public func attemptSetBeginDate(setDate :Date){
        if earliestDate.compare(setDate) == ComparisonResult.orderedAscending{
            beginDate = setDate
        }
        else{
            resetValues()
        }
        //Update the string values
        updateBeginDateEndDateStringValues()
    }
    
    /**
     * This function will reset the begin date to the default date
     */
    public func resetValues(){
        beginDate = Date()
        updateBeginDateEndDateStringValues()
    }

    /**
     * This function will get a ScheduleModel object that the picker currently has set.
     */
    public func getScheduleModel() -> ScheduleModel{
        //Return a ScheduleModel object with the info that was set by the user with the picker
        return ScheduleModel(beginDate: beginDate, duration: durationInDays)
    }
    
    /**
     * This function takes the current values in the beginDate and dateGap and sets the beginDateStringValue and endDateStringValue to the correct string values. If the end date is not available, we will set the end date the same as the begin date
     */
    private func updateBeginDateEndDateStringValues(){
        //Set the begin date String value
        beginDateStringValue = DateUtil.convertDateToString(date: beginDate, format: DATE_FORMAT)        //Lets add dateDate to the begin time and convert the result to string to set to endDateStringValue
        if let endDate = DateUtil.addDaysToDate(days: durationInDays, date: beginDate){
            //Set the string values of the end date
            endDateStringValue = DateUtil.convertDateToString(date: endDate, format: DATE_FORMAT)
        }
        else{//If the end date is not available, set it to the begin date
            endDateStringValue = beginDateStringValue
        }
        
        //Alert the delegate
        alertDelegate();
    }
        
    /**
     * This function will set the time zone with the given abbreviation.
     *
     * @param The String abbreviation of the time zone you would like to set the timeZone property with.
     */
    private func setTimeZone(abbreviation :String){
        //Set the time zone with the given abbreviation
        timeZone = TimeZone(abbreviation: abbreviation)
    }
}
