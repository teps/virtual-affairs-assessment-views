//
//  ViewController.swift
//  Virtual Affairs Assessment - Views
//
//  Created by Terrick Mansur on 12/2/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import UIKit

class ScheduleTableController: UIViewController, ViewModelDelegate, UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    //All the six cells that will be displaying on the table view
    enum CellTypes : Int{
        case BEGIN_DATE_CELL = 0
        case BEGIN_DATE_PICKER_CELL = 1
        case FREQUENCY_CELL = 2
        case FREQUENCY_PICKER_CELL = 3
        case END_DATE_CELL = 4
        case END_DATE_PICKER_CELL = 5
    }
    
    //The contrainst of the pickers
    @IBOutlet weak var beginDatePickerHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var frequencyHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var endDatePickerHeightContraint: NSLayoutConstraint!

    //All the labels that displays the values
    @IBOutlet weak var beginDateValue: UILabel!
    @IBOutlet weak var frequencyValue: UILabel!
    @IBOutlet weak var endDateValue: UILabel!
    
    //The cell views
    @IBOutlet weak var beginDateView: ClickableView!
    @IBOutlet weak var frequencyView: ClickableView!
    @IBOutlet weak var endDateView: ClickableView!
    
    //The pickers
    @IBOutlet weak var beginDatePicker: UIDatePicker!
    @IBOutlet weak var frequencyPicker: UIPickerView!
    @IBOutlet weak var endDatePicker: UIDatePicker!

    //The view model for this schedule table view controller
    var viewModel = ScheduleWithEndDateFrequencyViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set the viewModel its delegate to self
        viewModel.delegate=self
        //Collapse all the pickers
        collapseAllContraints()
        //Update the UI elements
        updateUIElements()

        //Set the tap gestures
        let editBeginTap = UITapGestureRecognizer(target: self, action: #selector(editBeginDate))
        beginDateView.addGestureRecognizer(editBeginTap)
        let editFrequencyTap = UITapGestureRecognizer(target: self, action: #selector(editFrequency))
        frequencyView.addGestureRecognizer(editFrequencyTap)
        let editEndDateTap = UITapGestureRecognizer(target: self, action: #selector(editEndDate))
        endDateView.addGestureRecognizer(editEndDateTap)

        //self as the freq pickerview data source
        frequencyPicker.dataSource=self
        frequencyPicker.delegate=self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     * The action that the user has selected a begin date
     */
    @IBAction func didSelectBeginDate(picker: UIDatePicker) {
        //Try to set the begin date
        viewModel.attemptSetBeginDate(setDate: picker.date)
    }
    
    /**
     * The action that the user has selected an end date
     */
    @IBAction func didSelectEndDate(picker: UIDatePicker) {
        viewModel.setEndDate(endDate: picker.date)
    }
    
    
    @objc func editBeginDate() {
        viewModel.openEditBeginDate()
    }

    @objc func editFrequency(){
        viewModel.openEditFrequency()
    }

    @objc func editEndDate(){
        viewModel.openEditEndDate()
    }
    
    /**
     * Implementing this function will notify that the beginDateString values or/and the endDateStringValue has changed
     */
    func valuesDidChange(){
        //Reload the table view to show the new data
        updateUIElements()
    }
    
    /**
     * The user clicked the clear button, clear the view model
     */
    @IBAction func clearClicked(_ sender: Any) {
        //Clear all
        viewModel.clear()
    }
    
    /**
     * This function will collapse all the contraints
     */
    private func collapseAllContraints(){
        beginDatePickerHeightContraint.constant=0
        frequencyHeightContraint.constant=0
        endDatePickerHeightContraint.constant=0
    }
    
    /**
     * This function will update all the UI elements in this view
     */
    private func updateUIElements(){
        
        
        //Set the values of the pickers
        beginDatePicker.minimumDate=viewModel.earliestDate
        beginDatePicker.setDate(viewModel.beginDate, animated: true)

        endDatePicker.minimumDate=viewModel.beginDatePlusFrequency
        endDateView.isHidden=(viewModel.endDate == nil) ? true : false
        endDatePicker.isHidden=(viewModel.endDate == nil) ? true : false

        if let endDate = viewModel.endDate{
            endDatePicker.setDate(endDate, animated: true)
        }
        
        self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.2, animations: {
            //Update the labels with the viewmodel values
            self.beginDateValue.text=self.viewModel.beginDateStringValue
            self.frequencyValue.text=self.viewModel.frequencyStringValue
            self.endDateValue.text=self.viewModel.selectedEndString
            //Update the contraints
            self.beginDatePickerHeightContraint.constant = (self.viewModel.editingBeginDate) ? 216 : 0
            self.frequencyHeightContraint.constant = (self.viewModel.editingFrequencyDate) ? 216 : 0
            self.endDatePickerHeightContraint.constant = (self.viewModel.editingEndDate) ? 216 : 0
            //self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        })
    }
    
    /**
     * This function takes in an index and return to you the cell identifier that the table has to use for the cell position. 
     *
     * @param: The index of the cell you are looking for.
     * @return: The identifier for that cell.
     */
    private func getCellIdentifierAndClassAtIndex(index : Int) -> String{
        switch index {
            case CellTypes.BEGIN_DATE_CELL.rawValue, CellTypes.FREQUENCY_CELL.rawValue, CellTypes.END_DATE_CELL.rawValue:
                return "labelValueCell"
            case CellTypes.BEGIN_DATE_PICKER_CELL.rawValue, CellTypes.END_DATE_PICKER_CELL.rawValue:
                return "datePickerCell"
            case CellTypes.FREQUENCY_PICKER_CELL.rawValue:
                return "frequencyPickerCell"
            default:
                return ""
        }
    }
    
    // MARK - Pickerview data source
    /**
     * This function is called when the user has made a selection with the picker
     */
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        //Tell out view model that a selection was made
            viewModel.setFrequency(frequencyInt: row)
    }
    
    /**
     * This pickerview only has one component, no just return 1 always
     *
     * @return: The number of components we want
     */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    /**
     * This fucntion returns the title the picker view would like to show at the given row.
     *
     * @param: The title to show at that row
     */
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //Return the string at that index
        return viewModel.OPTIONS[row]
    }
    
    /**
     * This function return the number of options out frequency picker has.
     *
     * @return: The number of options
     */
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //Return the option count in our ViewModel
        return viewModel.FREQUENCY_VALUES.count
        
    }
}

