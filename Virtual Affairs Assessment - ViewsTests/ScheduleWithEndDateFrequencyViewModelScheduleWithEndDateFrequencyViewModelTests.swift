//
//  ScheduleWithEndDateFrequencyViewModel.swift
//  Virtual Affairs Assessment - Views
//
//  Created by Terrick Mansur on 12/2/16.
//  Copyright © 2016 Terrick Mansur. All rights reserved.
//

import XCTest

class ScheduleWithEndDateFrequencyViewModelTests: XCTestCase {
    
    let march_20_9999 = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 20, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

    let march_22_9999 = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 22, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

    let march_28_9999 = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 28, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

    
    var viewModel = ScheduleWithEndDateFrequencyViewModel()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     * This test tests if the set frequency func has set the frequency property correctly
     */
    func testSetFrequency() {
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Daily)
        XCTAssertEqual(viewModel.frequency, ScheduleWithEndDateFrequencyViewModel.Frequency.Daily, "Frequency was not properly set.")
    }
    
    /**
    * This test tests if the set frequency has calculated the bedginDateplusFrequency corectly
    */
    func testSetFrequencyBeginDatePlusFrequencyCalculation_OneMonth(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Monthly)
        let april_20_9999 = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 4, day: 20, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))

       XCTAssertEqual(viewModel.beginDatePlusFrequency, april_20_9999, "beginDatePlusFrequency was not calculated correctly with one month")
    }
    
    /**
     * This test tests if the set frequency has calculated the bedginDateplusFrequency corectly
     */
    func testSetFrequencyBeginDatePlusFrequencyCalculation_OneWeek(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Weekly)
        let march_27_9999 = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 27, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))
        
        XCTAssertEqual(viewModel.beginDatePlusFrequency, march_27_9999, "beginDatePlusFrequency was not calculated correctly with one week")
    }
    
    /**
     * This test tests if the set frequency has calculated the bedginDateplusFrequency corectly
     */
    func testSetFrequencyBeginDatePlusFrequencyCalculation_OneDay(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Daily)
        let march_21_9999 = Calendar.current.date(from: DateComponents.init(calendar: Calendar.current, timeZone: TimeZone.current, era: nil, year: 9999, month: 3, day: 21, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil))
        
        XCTAssertEqual(viewModel.beginDatePlusFrequency, march_21_9999, "beginDatePlusFrequency was not calculated correctly with one day")
    }

    /**
     * This test tests if the set frequency has calculated the bedginDateplusFrequency corectly
     */
    func testSetFrequencyBeginDatePlusFrequencyCalculation_Once(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Once)

        XCTAssertNil(viewModel.beginDatePlusFrequency, "beginDatePlusFrequency was not set to null when set to frequency was set to null")
    }
    
    /**
     * This test tests if the end date is set when we attempt to set it
     */
    func testSetEndDate_withInValidDate(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Weekly)
        viewModel.setEndDate(endDate: march_22_9999!)
        
        XCTAssertEqual(viewModel.endDate, viewModel.beginDatePlusFrequency, "We cannot set the end date before beginDatePlusFrequency. If we try we need to set it to beginDatePlusFrequency.")
    }
    
    func testSetEndDate_withValidDate(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setFrequency(frequency: ScheduleWithEndDateFrequencyViewModel.Frequency.Weekly)
        viewModel.setEndDate(endDate: march_28_9999!)
        
        XCTAssertEqual(viewModel.endDate, march_28_9999, "The end date should be set with the given value it it is after beginDatePlusFrequency")
    }
    
    func testSetEndDate_NullBeginDatePlusFrequency(){
        viewModel.attemptSetBeginDate(setDate: march_20_9999!)
        viewModel.setEndDate(endDate: march_28_9999!)
    
        XCTAssertNil(viewModel.endDate, "The end shout not have a value if frequency is at Once and beginDatePlusfrequency is at null")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
